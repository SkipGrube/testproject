class Weapon:
  
  location = 0
  attackMod = 0
  defenseMod = 0
  name = ""
  
  
  def __init__(self, name, location, attackMod, defenseMod):
    self.name = name
    self.location = location
    self.attackMod = attackMod
    self.defenseMod = defenseMod
    return 0
