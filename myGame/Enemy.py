class Enemy:
  
  location = 0
  hp = 0
  attack = 0
  defense = 0
  name = ""
  
  def __init__(self, name, hp, location, attack, defense):
    self.name = name
    self.hp = hp
    self.location = location
    self.attack = attack
    self.defense = defense



  def debugEnemy(self):
    print("Enemy data ::")
    print("Name == " + str(self.name) + "   location == " + str(self.location) + "   hp == " + str(self.hp))
