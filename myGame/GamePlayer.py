
# A class is an object.  We can create multiple instances (copies) of it
class GamePlayer:
  # These are values inherent to the object.  We can refer to them within this class as "self.value" (ex: self.hp = self.hp - 5)
  # Refer to them outside the class as "objectName.value" (ex: joe.hp = joe.hp - 10)

  name = ""
  hp = 0
  location = 0
  attack = 0
  defense = 0


  
  def __init__(self, name, hp, location, attack, defense):
    # Assign the name to this object
    self.name = name
    self.hp = hp
    self.location = location
    self.attack = attack
    self.defense = defense
       
    print("New player being created....")
    self.DisplayStats()
  
  

  def TakeDamage(self, damage):
    self.hp = self.hp - damage
    print("Took damage!")
    self.DisplayStats()
    
        

  def DisplayStats(self):
    print("")
    print("Name :: " + str(self.name))
    print("HP :: " + str(self.hp))
    print("")
    return 0
