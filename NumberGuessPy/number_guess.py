#!/usr/bin/env python3

import random

print("Welcome to the Number Guess Game!\n\n\n")


playerCount = input("Please enter number of players: (1-5) :  ")
playerCount = int(playerCount)
if playerCount > 5:
	print("Please enter a value less than 6")
    
elif playerCount <= 5:



	maxNumber = input("Please enter the maximum value for the secret number: ")
	maxNumber = int(maxNumber)



	secretNumber = random.randrange(1, maxNumber + 1)


	playerGuess = 0
	playerTurn = 1

	while playerGuess != secretNumber:
                print("Player # " + str(playerTurn) + " , what's your guess? ")
                playerGuess = input(" : ")
    
                playerGuess = int(playerGuess)
  
                if (playerGuess > secretNumber):
                    print("Sorry, your guess of " + str(playerGuess) + " was too high!\n")

   
                if (playerGuess < secretNumber):
                    print("Sorry, your guess of " + str(playerGuess) + " was too low!\n")

                if(playerGuess == secretNumber):
                    print("CONGRATULATIONS!!!  Player " + str(playerTurn) + "  GUESSED IT CORRECTLY!")
                    print("The secret number was : " + str(secretNumber))

                
                if(playerTurn >= playerCount):
                    playerTurn = 0

                
                
                playerTurn= playerTurn +1



                #print("CONGRATULATIONS!!!  Player " + str(playerTurn) + "  GUESSED IT CORRECTLY!")
                #print("The secret number was : " + str(secretNumber))


  
  


